== Om bakgrunnen for undersøkelsen

=== Innledning

Formålet med denne rapporten er å presentere en undersøkelse om
virkninger av lov om angrefrist ved visse avtaler om forbrukerkjøp som
Stortinget vedtok i mars 1972. Undersøkelsen bygger i hovedsak på en
intervjuundersøkelse av 2029 forbrukere foretatt av Norsk Gallupp A/S
i månedene juni, august og september 1974.

Undersøkelsen bygger på et forprosjekt gjennomført som diplomoppgave
ved Bedriftsøkonomisk institutt i 1973 foretatt av Erik Strøm og Thor
Rogan. Forprosjektet omfattet utarbeiding av spørreskjema og utprøving
av det metodiske opplegg for undersøkelsen. På enkelte punkter er det
også tatt med materiale innsamlet ved en postenkete av Marketing Link
A/S for Direktesalgsforbundet høsten 1977footnote:[ROGAN, Thor og
STRØM, Erik. «Effekter av angrefristloven. En forprosjektrapport»,
Forbruker- og administrasjonsdepartementet, Oslo 1974.  Postenketen er
foretatt for Direktesalgsforbundet av Marketing Link a.s og
dokumentert slik: Rapport/direktesalg - dørsalg. Forbrukerundersøkelse
september 1977. Oslo, 27.  oktober 1977. 29 sider.  Analyserapport for
forbrukerundersøkelse direktesalg/dørslag, Oslo, 8.november 1977. 6
sider.  Direktsalg/dørsalg.  Tilleggsrapport til forbrukerundersøkelse
september 1977. 0slo, 28. november 1977. 3 sider.].

Undersøkelsen omfatter en beskrivelse av omfanget av salg utenfor fast
salgssted, forbrukernes syn på disse salgs- formene og hvilke
erfaringer de har gjort ved slike kjøp. For å vurdere lovens
virkninger undersøkes forbrukernes lovkunnskap og, så langt det er
mulig, i hvilken grad forbrukerne benytter de rettigheter loven skal
gi.

Prosjektet er finansiert i fellesskap av Justisdepartementet og
Forbruker- og administrasjonsdepartementet.

Prosjektopplegget ble blant annet drøftet med forskere på området og
vurdert av NAVF og Statistisk Sentralbyrå.

De faglige grunnene til å velge ut denne loven for nærmere
undersøkelser er flere:

 * loven har avgrenset virkeområde og er lovteknisk enkel
 * en del av lovens mulige virkninger kan registreres med en velprøvet
   samfunnsvitenskapelig metode: en intervjuundersøkelse
 * i Norge foreligger det studier som kan tjene som basis for å
   undersøke slike virkninger. Spesielt vises det til undersøkelsen av
   den norske hushjelplov «En lov i søkelyset» og til «Rettssosiologi»
   av Vilhelm Aubert.footnote:[Aubert, Eckhoff, Sveri: «En lov i
   søkelyset». Oslo 1952, Eskeland og Finners: «Rettshjelp», Oslo
   1973, Aubert, Vilhelm: «Rettssosiologi» Universitetsforlaget, Oslo
   1968 og «Rettens sosiale funksjon», ISBN 8200015238,
   Universitetsforlaget. Oslo 1978.]

Planlegging av denne rapport ble basert på disse to studier
og utkastet til NOU 1973:52 «Om administrative og Økonomiske
konsekvenser av lovgivning m.v.».

Prosjektet som formuleres som
departementsoppdrag bør ha en konkret nytteverdi. I planleggingen ble
det vist til følgende mulige betydning av undersøkelsen:

 * den kan vise i hvilken grad loven er kjent og benyttet av kjøper
   og selger om tilsiktede mål er nådd og om eventuelle
   ikke-tilsiktede virkninger har oppstått,
 * undersøkelsen kan gi grunnlag for tiltak for å gjøre loven bedre
   kjent blant de grupper loven omfatter, og
 * resultatene vil kunne gi grunnlag for å vurdere om loven i sin
   nåværende form vil trenge en revisjon for bedre å nå de oppsatte
   mål.
 * Undersøkelsens vitenskapelige betydning ligger i en utvidelse av de
   empiriske grunnlaget på området lovgivningens virkninger.

=== Oversikt over innholdet

Rapporten er disponert slik:

Kapittel 1 gir bakgrunnen for rapporten og gir en orientering om
rammen for arbeidet med erfaringstilbakeføring fra lovverket i
sentralforvaltningen.

Kapittel 2 gir en kort oversikt over den prosess som førte til at det
ble innført en egen lov om angrefrist for visse kjøp og noen av de
synspunkter og vurderinger som ble lagt til grunn.

Kapittel 3 gjennomgår de generelle og konkrete mål for dette
lovtiltaket og de virkninger en kan forvente. Videre risser kapitlet
opp endel av de årsakssammenhenger det kan være aktuelt å se nærmere
på.

Kapittel 4 drøfter valg av metodisk opplegg for undersøkelsen og
hvilke utvalg av forbrukere den bør bygge på.

I kapittel 5 gjennomgås: direktesalgstilbudene til forbrukerne og
hvilke holdninger forbrukere: har til disse salgsformene.

Kapittel 6 tar opp forbrukernes kjøpserfaringer og hvilken sammenheng
det er mellom disse erfaringene, holdningen til direktesalg og hvor
ofte folk får slike tilbud.

Forbrukernes lovkunnskap er gjennomgått i kapittel 7 og vekten
er lagt på å analysere i hvilken grad selgernes informasjonsplikt
fører til økt lovkunnskap hos den som har kjøpt utenfor fast
salgssted.

Kapittel 8 omhandler tidspunktene for betaling og levering
av varen.

I kapittel 9 foretas det så en samlet oppsummering av hoved-
funnene i undersøkelsen ut fra målene i kapittel 3. De mulige
forbrukerpolitiske konsekvenser er gjennomgått i kapittel 10.

=== Administrative og økonomiske konsekvenser av lover m.v.

Følgende prinsipielle syn er understreket i utredningen om
«Administrative og økonomiske konsekvenser av lover m.v.»footnote:[NOU
1973:52 «Om administrative konsekvenser av lovgivning m.v.», ISBN
8200701069, side 15, https://urn.nb.no/URN:NBN:no-nb_digibok_2011103106006]:

[quote]
«Det må være en klar forutsetning at det offentlige regelverk skal
virke overensstemmende med de respektive bestemmelsers
målsetting. Dette gjelder både planlagte bestemmelser og de som før
lengst er vedtatt. Denne forutsetning stiller krav til forberedelse,
form, forvaltningsapparat m.v. og nødvendiggjør en stadig observasjon
av gjeldende lover og andre regler.»

Og lovgivning er en kontinuerlig prosess, som i utredningen ble
oppsummert i følgende figur:

.Lovgivning som en kontinuerlig prosess
image::images/lovgivingsprosess.svg[scaledwidth=70%,align="center"]

«Veiledende retningslinjer til bruk for departementene i arbeidet med
det offentlige regelverk» ble fastsatt ved kongelig resolusjon av
31. oktober 1975.  Punkt 10 omhandler «observasjon, rapportering,
erfaringstilbakeføring»:

[quote]
«Når bestemmelsen er trådt i kraft, skal fagdepartementet holde seg
orientert om hvordan de virker. Dette kan skje både gjennom egne
tiltak og ved å 'sørge for at underliggende organer har bestemmelsen
under observasjon og gir rapportering om virkningene. Det
erfaringsmaterialet som departementet får på denne måte, skal det
anvende i det løpende arbeid med ajourføring og justering av
regelverket. Større lover og andre viktige regelverk skal tas opp til
fornyet vurdering med jevne mellomrom, ikke sjeldnere enn hvert
5. år.»

Virkningene av offentlige regelverk kan undersøkes på forskjellige
måter. I kommentarene til punkt 10 skisseres følgende muligheter for
tilbakeføring av erfaring:

 * muntlig som skriftlig kontakt med publikum
 * opplysninger og reaksjoner gjennom presse, radio og fjernsyn
 * inspeksjonsordninger, møter med tjenestemenn fra den ytre etat
 * kontakt med grupper som har vanskelig for å fremme sine krav.

Søkelyset må også fra tid til annen rettes mot de tilfelle hvor man
intet hører om lovers- og andre bestemmelsers virkninger. Slik taushet
kan være et varsel f.eks. om at en lov ikke  er tilstrekkelig kjent,
eller at de som loven angår ikke vet hvordan de skal bringe sine
problemer fram.

Vilhelm Aubert understreker at det er nærliggende i rettssosiologien å
undersøke i hvilken grad lovene har ført til samsvar mellom norm og
atferd, eller om de har preget folks innstillinger og
rettsoverbevisningfootnote:[AUBERT, Vilhelm: «Rettsosiologi». Side
164.].

Den undersøkelsen som er gjengitt i de følgende kapitler må sees som
et ledd i det praktiske og faglige arbeidet for å få bedre kunnskaper
om virkninger av lovgivning.



