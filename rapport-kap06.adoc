== Erfaringer med direktesalg

=== Innledning

Dette kapittel søker å beskrive de erfaringer folk har: Hvilke vare de
har kjøpt og hvor fornøyd de er med denne handelen.

=== Varer, priser, salgsform og kjøpergrupper

Et utvalg på 732 forbrukere fikk spørsmål om sine erfaringer.
Utvalgskriteriet var at de svarte bekreftende på at «de i løpet av
siste år har kjøpt varer ved dørsalg, på messer, spesielle bussturer,
ved selskapssalg eller lignende og betalt en tid etter bestillingen.»

[cols="70,~,~"]
.Varer kjøpt ved direktesalg
|===
|                                 | Andel | Antall svar
| Bøker                            |  32% | 236
| Støvsuger                        |   8% |  58
| Tekstiler, sko                   |   8% |  57
| Kjøkkenutstyr                    |   8% |  56
| Møbler, tepper, persienner       |   7% |  51
| Andre husholdningsmaskiner       |   5% |  36
| Sminke, parfyme, toalettartikler |   5% |  36
| Symaskiner                       |   4% |  28
| Mat                              |   4% |  27
| Parykker, postisjer              |   1% |   5
| Vet ikke                         |   3% |  21
| Andre varer                      |  18% | 135
| *Sum*                            | 103% | 746*)
| Prosentbasis                     | 100% |732 kjøpere
3+| *) For 14 av kjøperne er 2 svar registrert.
|===

[cols="70,~,~"]
.Kjøp etter salgsform
|===
|      | Antall svar | Andel
| Dørsalg      | 434 | 59%
| Selskapssalg | 141 | 19%
| Messe        | 110 | 15%
| Andre former |  38 |  5%
| Buss	       |   3 |  0,5%
| Vet ikke     |  15 |  2%
| *Totalt*   | 741*) | 100.5%
| Prosentbasis | 734 kjøpere | 100%
3+| *) 7 har avgitt to svar.
|===

[cols="70,~,~"]
.Kjøpesummer
|===
|                    | Antall kjøp | Andel
| Mindre enn 200 kr  |         333 |  45%
| 200 — 500 kr       |         180 |  25%
| 500 - 1 000        |          99 |  14%
| Mer enn 1 000 kr   |          96 |  13%
| Husker ikke        |          24 |   3%
| Sum                |         732 | 100%
|===

Tabell 6.1 viser at omtrent tredjedelen av salgene er bøker. Den neste
tredjedelen av kjøpene er delt likt mellom støvsugere, tekstiler/sko,
kjøkkenutstyr og møbler/tepper/persienner. Den gjenstående andel er
delt mellom flere varegrupper, der «andre varer» utgjør hoveddelen.

Som en kan vente av salgstilbudene, står dørsalgene for ca. 60% av
kjøpene. Antallet kjøp er størst for de laveste kjøpesummene. Vel en
fjerdedel av kjøpesummene er på mer enn 500 kr.

64% av kjøperne er kvinner. Delvis skyldes dette flertallet at
selskapssalgsformen først og fremst er rettet not kvinner. Holdes
denne salgsformen utenfor, blir andelen kvinner 5% Nenne
overrepresentasjonen på 5% kan skyldes at kvinner er mer
tilstedværende i husholdningene enn menn og møter
direktesalgstilbudene oftere. (Jfr. Mål 11). :Kjønnsrollemønsteret
avspeiles i at andelen «hovedforsørgere» blant kjøperne bare er 39%,
mens den er 48% i det representative utvalget. Kjøpesummene er noe
høyere for mennene i utvalget enn for kvinnene. 57% av mennene og
ca. 48% av kvinnene oppgir kjøpesummer på over 200 kr.

Bakgrunnsvariablene utdanningstid,
yrke, inntekt, kommunetype, befolkningstetthet og alder viser bare små
forskjeller mellom gruppen av kjøpere og det representative utvalget.


=== Anger og misnøye

Lovutvalget tok både sikte på «situasjoner selgeren har opptrådt med
utilbørlig press og overrumplingsmetoder og der hvor selgeren ikke så
mye er å bebreide, men hvor salget hovedsaklig skyldes en uoverveiet
innskytelse hos kjøperen».

Skal forbrukeren kunne nyttiggjøre seg en «angrefrist» må det
foreligge et ønske om å gå tilbake på en bestilling eller et
kjøp. Denne «anger» er den nøkkel som lovens bestemmelser bygger
på. Ved passivitet fra kjøperens side blir avtalen bindende.

Lovutvalget søkte å vite hvor utbredt et slikt ønske eller anger var,
og fikk utført en egen intervjuundersøkelse i 1968. Den undersøkelsen
viste at 19% av dem som hadde foretatt kjøp på døren siste år,
oppga at de hadde ønsket å heve dette kjøpet. Noe under halvparten av
dem som ønsket å heve, anførte grunner som tydet på at salget hadde
prea av overrumpling fra selgerens side eller at de ikke hadde fått
tenkt seg om.

En kombinasjon av spørsmål i undersøkelsen 6 år etter, tar sikte på å
avklare det samme: Ønsket om å gå tilbake på en bestilling eller et
kjøp. En rekke utenforliggende faktorer påvirker de svar en får om
dette. Da vi spør om tidligere kjøp kan problemstillingen være
uaktuell - forhold ved kjøpet hører fortiden til. Videre kan det være
vanskelig å innrømme at man har foretatt noe som kan omfattes som «en
uoverveiet innskytelse», selv overfor en nøytral
intervjuer. Avslutningsvis kan en «anger» ha liten betydning for
intervjupersoner fordi kjøpesummen var lav. Slike forhold må tas med i
betraktningen når spørsmålene formuleres.

Man må imidlertid gå ut fra at intervjupersonene svarer så riktig de
kan på de spørsmålene som er stilt.  Erfaringene fra
intervjuundersøkelser bekrefter at opplysninger om tidligere atferd
kan væra mindre avhengige av slike utenforliggende forhold som svar på
vurderings/holdningsspørsmål. Spørsmål av begge kategorier er derfor
benyttet for å kunne gi en oversikt over omfanget av «anger». Tre
spørsmål registrerer tidligere atferdsuttrykk for mulig misnøye - og
kjøperens egen vurdering av kjøpet på intervjutidspunktet:

1. Hvis man bekrefter at man ville kjøpe samme vare igjen på samme
   måte, burde det være et uttrykk for at man var fornøyd med kjøpet
   og ikke «angret». Vi har derfor spurt om hva kjøperne ville gjøre
   om de fikk tilbudet en gang til og igjen kunne velge: Ville de da
   kjøpe varen på denne måten, kjøpt vare i butikk eller kanskje
   utsatt eller ikke foretatt kjøpet? 54% av utvalget ville ha
   kjøpt igjen på denne måten, 10% ville ha kjøpt på et annet
   sted. 17% ville ha utsatt eller ikke foretatt kjøpet, mens
   vet-ikke gruppen utgjorde 19%.

2. Et konkret uttrykk for usikkerhet ved kjøpet ville være om en
   vurderte kjøpet sammen med ektefelle, nabo eller andre i løpet av
   angrefristen med tanke på eventuell avbestilling. 36%
   foretok en slik vurdering, 60% gjorde det ikke.

3. Et utsagn om at en ikke er helt fornøyd med kjøpet, vil også kunne
   gi en indikasjon på om man ville benytte seg av en angrefrist. 15%
   av utvalget bekreftet at de «ikke var helt fornøyd med et slikt
   kjøp» siste år. Hvis vi bare ser på det siste kjøpet var 9%
   «ikke helt fornøyd».

I Marketing Links 1977-undersøkelsefootnote:[Undersøkelsen er
foretatt for Direktesalgsforbundet av Marketing Link a.s og
dokumentert i tre rapporter: (1) Rapport/direktesalg - dørsalg
Forbrukerundersøkelse september 1977, Oslo, 27. oktober 1977 29
side. (2) Analyserapport. for forbrukerundersøkelse direkte
salg/dørsalg, Oslo, 8. november 1975 6 side.  (3)
Direktesalg/dørsalg. Tilleggsrapport til forbrukerundersøkelse
september 1977. Oslo, 28. november 1977 3 side.] er det stillet et
spørsmål som gir en vurdering av kjøpserfaringer. Spørsmålet er
knyttet til siste kjøp og svarfordelingen vises i tabell 6.4 sammen
med fordelingen i vår undersøkelse:

.Kjøpsvurdering siste kjøp
|===
|                       2+| Postenkete 1977 2+| Kjøpere
| Gjorde en meget god handel | 4%  .3+.^| 67%  2+| Har det hendt at De ikke har vært helt fornøyd med et slikt kjøp?
| En god handel            | 23%         .2+.^| 82% .2+| Nei
| Hverken god eller dårlig | 40%
| En dårlig handel         | 5%  .2+.^| 8% .2+.^| 9% .2+| Ja
| En meget dårlig handel   | 3%
| Ubesvart                 | 25%    |    | 9% | Ubesvart
| *Totalt*                 | 100%   |    | 100% | Totalt
| Prosentbasis             | 780    |    | 732 | Prosentbasis
|===

Sammenstillingen i tabell 6.4 viser at andelene som «ikke er helt
fornøyd» og har gjort en «dårlig handel» er nesten identiske, 8% og 9%
Postenketsundersøkelsen har en høyere «vet ikke»- prosent som dels kan
skyldes ulik intervjuform og at dels færre i 1977 har en positiv eller
nøytral kjøpsvurdering.

I tabell 6.5 er det foretatt en sammenregning av svarene på
spørsmålene om gjenkjøp, avbestillingsvurdering og om en ikke var helt
fornøyd. Grunnlaget for sammenregningen er gitt i vedlegg 1.

For å få et samlet mål på hvor fornøyd man er med varen er svarene
regnet sammen. De som ikke ville kjøpe varen igjen på samme måten -
som vurderte avbestilling samen med andre eller «ikke har vært helt
fornøyd» med kjøpet er betegnet som «misfornøyde». De som har svart at
de ville kjøpe igjen, at de ikke vurderte avbestilling og avkrefter at
de «ikke er helt fornøyde» klassifiseres i gruppen «positive».  Andre
svarkombinasjoner kommer da mellom disse ytterpunktene, slik det
fremgår i vedlegg 1. Bare svar som gjelder siste kjøp er tatt med i
denne indeksen.

[cols="25,10,~,~,~"]
.Kjøpsmisnøye
|===
3+|                | Antall svar | Andel
.2+| Fornøyde    | 0 | +++ | 238 |  33%
                 | 1 | -++ | 349 |  48%
.2+| Misfornøyde | 2 | --+ | 118 |  16%
                 | 3 | --- |  27 |   4%
| *Sum*          |   |     | 732 | 101%
|===

Tabell 6.5 viser at kjøperne stort sett gir uttrykk for at de er
fornøyde med det kjøpet. 20% kan ikke beteanes som fornøyde når
svarene regnes sammen. For kjøp ved døren er misnøyen noe høyere enn
for de andre salgsformene, 24% For de andre salgsformene er andelen
fra 14% til 16%.

Misnøyen med kjøpet er fra 3% til 7% over gjennomsnittet på 20% for
symaskiner, bøker, støvsugere og møbler/tepper/persienner. Kjøpere av
sminke/ parfyme/toalettartikler, andre husholdningsmaskiner og
kjøkkenutstyr er mer fornøyde, fra 7% til 14% under
gjennomsnittet. De resterende varegrupper, mat og parykker/postisjer
ligger nær gjennomsnittet.

Er det først og fremst de rimelige varer kjøperne er misfornøyd med?
Tabell 6.6 gir en oversikt over kjøpsmisnøye etter varens pris.

[cols="50,~,~"]
.Misnøye etter varens pris
|===
|                    | Andel misfornøyde | Antall
| Kjøpesum under 200 kr | 14% | 333
| 200-500 kr            | 28% | 180
| 501-1000 kr 		| 25% | 99
| Mer enn 1000 kr	| 15% | 96
| Vet ikke     		| 29% | 24
| *Gjennomsnitt*	| 20% | 732
|===

Misnøyen er størst for varer i prisgruppen fra 200 til 1 000 kroner,
og synker igjen for kjøpesummer over 1 000 kroner.
 
Kjøpsmisnøyen er noe over gjennomsnittet i aldersgruppen 20 til 39 ar,
og da lavere for aldersgruppene før og etter denne perioden. Det er en
svak tendens til at kvinner er mer misfornøyde enn menn (+4%).

Det er forskjeller mellom kjøpere i spredt/tettbygde områder, men
mønsteret er ikke konsistent. Materialet viser heller ikke på dette
punkt noen forskjell etter husstandens inntekt.

De 110 som «ikke var helt fornøyde» med sitt kjøp ble bedt om å oppgi
årsakene til sin misnøye. 20 oppgir at varen ved nærmere ettertanke
var for dyr, 39 at varen ikke holdt hva den lovet, 13 at de kunne
kjøpt bedre/ billigere et annet sted, 13 at de ikke hadde bruk for
varen og 19 at varen var beheftet med feil. 13 ga andre årsaker og 5
svarte vet ikke.

Hvis varen var beheftet med feil, kan dette være en mangel etter
kjøpslovens bestemmelser. 19 av de 110 begrunner sitt svar med feil
ved varen. 75 peker på forhold som angrefristloven vel tar sikte på å
dekke. Forskjellen i samlet kjøpsmisnøye, slik den defineres i
indeksen, viser ikke noe forskjell mellom disse to gruppene. For de
resterende 18 foreligger det ikke spesifikke opplysninger om grunnen
til misnøye.

I kapittel 9 er det gitt en samlet fremstilling av kjøpsmisnøye,
lovkunnskap og betydningen av lovbestemmelsene.

Bruk av data som skal vise grad av tilfredshet hos kjøperne krever en
nærmere faglig drøfting. Svarer de muntlige rapporten til kjøpernes
faktiske opplevelser, eller med andre ord, er dataene valide? Om vi
godtar at de verbale utsagn avspeiler personenes tilfredshet eller
utilfredshet, kan det fortsatt være problemer med å tolke eller
vurdere slike data. Øhlander drøfter disse spørsmålene i artikkelen
«Skal formålet for forbrugerpolitikken være en tilfreds
forbruger?»footnote:[ØHLANDER, Folke. «Skal målet for
forbrugerpolitikken være en tilfredsforbruger» i Rask-Jensen og
Øhlander. «Forbrugerproblemer og forbrugerpolitikk».  Abertslund
1977. Særlig side 268-831.]. Vanskelighetene med å fortolke og å bruke
tilfredshetsmålinger til forbrukerpolitiske formål summeres opp i
følgende punkter:

 * Polyannahypotesen: At det i kommunikasjonen med andre (i dette
   tilfelle med intervjuere) er en menneskelig tendens til å anvende
   vurderingsmessig positive ord oftere og i flere sammenhenger enn
   vurderingsmessig negative ord.
 * Forventningseffekter: At de intervjuede muligens vil avgi svar, som
   ikke antyder noe om deres «sanne» tilfredshet, enten fordi de
   ønsker å glede undersøkeren - gir de svar de tro er ønsket - eller
   fordi de tror at det er i deres egen interesse å fremstille sine
   synspunkter uriktig.
 * Tilfredshet i relasjon til en standards Tilfredshet med et gitt
   ferdighetsnivå eller en gitt miljøtilstand forsvinner lett og
   fremkaller en ny og høyere standard som lykke/tilfredshet måles i
   forhold til.
 * Uvitenhet om alternative valgmuligheter.
 * Effektene av kognitiv dissonans på tilfredshet.
 * Manglende hensyntagen til langsiktige og samfunnsmessige
   betraktninger.

De av innvendingene ovenfor som berører validiteten kan både virke i
retning av en høyere og lavere registrering av
kjøpsmisnøye. Muligheten for en etterkontroll for om og i hvilken grad
den enkelte faktor ovenfor kan ha hatt innflytelse på svarene blir
begrenset blant annet av variabelvalget i undersøkelsen. Men en
grundig pretesting kan gi en Fore av hvordan forbrukerne oppfatter og
svarer på spørsmålene, om forholdet mellom etterkjøpsprosessen og de
svar de gir uttrykk for.  Forundersøkelsen ble gjennomført nettopp for
å få vurdert validitetsspørsmålene.

Vi kan vanskelig skaffe data om kjøpserfaringene på annen måte enn å
bygge på kjøpernes muntlige utsagn.  Å bygge svarene fra flere
spørsmål sammen i en indeks, skulle gi en større trygghet for at
kravet til validitet og også reabilitet bli oppfyltfootnote:[HELLEVIK,
0. side 259.]. Når det samles data om kjøpsmisnøye, er det ikke for å
gi bedre underlag for å oppnå forbrukerpolitiske eller
bedriftsøkonomiske ønskemål om «en så høj grad av tilfredsstillelse
som muligt for forbrukerne»footnote:[ØHLANDER, Folke.  side
265.]. Hensikten her er å belyse i hvilken grad subjektiv kjøpsmisnøye
eller «anger» utløser søking etter lovkunnskap slik angrefristsystemet
forutsetter (Mål 7 og 8). Manglende kunnskap om alternative
valgmuligheter (reell valgfrihet) og hensyn til langsiktige og
samfunnsmessige betraktninger blir drøftet i kapittel 10.

=== Rasjonelle kjøp

Rasjonelle kjøp forutsetter en viss planlegging. En bør ha en
formening om alternative produkter, priser og sitt eget behov for
produktet. En viktig innvending mot direktesalg er at initiativet til
salget ligger hos selgeren og ikke hos kjøperen. Muligheten til å
sammenligne varer og priser blir da begrenset.  Salgsoppleggene er
gjerne slik at forbrukerne må bestemmes seg med en gang, ofte med
henvisning til at produktet ikke kan skaffes på annen måte eller på >t
senere tidspunkt.

Bare tredjedelen av kjøperne foretok en sammenligning av varens pris
og egenskaper med tilsvarende produkter annet sted. Av disse 243 fant
41 den prisen de hadde betalt høy, 115 prisen middels og 37 syntes
prisen var lav.

Halvparten av kjøperne gir uttrykk for at de ikke hadde bestemte
planer om å kjøpe denne varen på forhånd. - Et spørsmål blir da om
ikke planlagte kjøp gir større kjøpsmisnøye enn planlagte kjøp. I
tabell 6.7 vises sammenhengen mellom kjøpsmisnøye og om. kjøpet var
planlagt.

[cols="24,4,~,~,~,~"]
.Kjøpsmisnøye etter planlegging
|===
| Kjøpsmisnøye: | | Planlagt | Ikke planlagt | Vet ikke | Alle kjøpere
.2+|Fornøyde | 0 | 41% | 27% | 3% | 33%
| 1 | 46% | 48% | 52% | 48%
.2+|Misfornøyde | 2 | 10% | 21% | 31% | 16%
| 3 | 3% | 5% | 3% | 4%
2+| *Sum* | 100% | 101% | 99% | 101%
2+| N  | (341) | (362) | (29) | (732)
2+| | 47% | 50% | 4% | 101%
|===

Tabellen viser at de som har planlagt kjøpet er mer fornøyde enn de
som ikke har foretatt slik planlegging.  Prosentandelen stiger fra 13%
til 26%, samtidig som andelen helt fornøyde synker fra 41% til
27%.

For symaskiner, støvsugere, møbler, kjøkkenutstyr og sminke er andelen
planlagte kjøp minst 10% over andelen på 47% som har planlagt sitt
kjøp. For varene mat, bøker og klær er andelen minst 10% lavere enn
gjennomsnittet. Denne forskjell kan skyldes salgsopplegg som kan
innebygge en form for planlegging. Eksempler på dette er selskapssalg,
der man ofte vet hva en blir tilbudt på forhånd, men på den annen side
likevel kanskje vanskelig kan avslå en invitasjon. For dyrere varer,
vil ofte flere besøk være nødvendige, slik at salgsprosessen skjer
trinnvis. For støvsugere kan man først sikre seg en avtale om
demonstrasjon «når ektefellen er til stede».  Bestillingen blir først
opptatt ved denne demonstrasjonen. Slike salgsopplegg er nok ikke
vanlig for bøker og matvarer.

Planleggingsnivået stiger jevnt med varens pris, fra 37% for
varer under 200 kroner til 17% for varer til over 1 000 kroner.

Det må tilføyes at av dem som oppgir at kjøpet var planlagt, sa bare
42% at de foretok pris og kvalitetssammenligning med tilsvarende
produkter annet sted. For dem som ikke hadde planlagt kjøpet synker
denne andelen til 19% 36.

En av hovedgrunnene for å foreslå loven var ifølge lovkomiteen at
«kjøperen befinner seg i en uforberedt situasjon uten mulighet til å
sammenligne pris og kvalitet med konkurrerende produkter». Nå må nok
denne formuleringen modereres noe selv om en kan konkludere med at
planleggingsnivået i de fleste hushold synes lavt.

En vurdering av prisene på de varer som tilbys ved disse salgsformene
må bygge på en egen undersøkelse av de varer og priser som tilbys på
hvert enkelt vareområde. Forbrukernes oppfatning av prisene kan ikke
direkte tas til inntekt for at prisene er spesielt fordelaktige eller
ufordelaktige. Så langt vi vet foreligger det ikke slike
undersøkelser.

En rekke kjøpere har henvendt seg til forbrukerorganet og
prismyndigheter med konkrete klager over prisene ved direktesalg.
Behandlingen av disse saker viser at prisnivået ved dørsalg for mange
produkter er høyere enn ved salg via andre kanaler. Grunnen er blant
annet de salgskostnadene som et direktesalgsapparat krever.  Mange
potensielle kunder skal kontaktes og det må bygges opp egne
salgsapparater knyttet til ett eller noen få produkter.

=== Holdninger salgsutsatthet, kjøp og kjøpserfaringer

==== Mulige konsekvenser

I dette avsnitt vil vi se nærmere på sammenhengene mellom holdningen
til direktesalg, salgsutsatthet og antall angrefristkjøp. De
forventede sammenhengene er gjengitt i fig. 3.2, og Å dette avsnittet
drøftes et utvalg av disse sammenhenger, gjengitt i fig. 6.1.

.Oversikt over mulige sammenhenger
image::images/mulige-sammenhenger.svg[scaledwidth=60%,align="center"]

I kapittel 3 ble disse sammenhengene formulert slikt:

Mål 13:: jo oftere en får direktesalgstilbud, desto mer skeptiske blir
forbrukerne til disse salgsformene

Mål 14:: en skeptisk holdning til direktesalg begrenser antallet slike
kjøp.

Mål 15:: økt salgsutsatthet gir flere kjøp

Mål 16:: kjøpserfaringene påvirker synet på direktesalg slik at gode
kjøpserfaringer gir en mer positiv holdning og dårlige kjøpserfaringer
gir en mer negativ holdning.

==== Salgsutsatthet og holdninger (Mål 13)

En kan vente at folk som stadig blir utsatt for direktesalgstilbud
etter hvert ble mer skeptiske til direktesalg enn folk som meget
sjelden fikk slike tilbud. Tabell 6.3 viser sammenhengen mellom
holdning og salgsutsatthet:

[cols="30,~,~,~,~,~"]
.Salgsutsatthet og holdning til direktesalg
|===
|                        5+^|Salgsutsatthet:
| Holdning til direktesalg: |   0p |   1p |    2 | 3/4p | Total
| Positiv/nøytral           |  13% |  11% |  10% |  15% | 12%
| Skeptisk                  |  54% |  63% |  71% |  48% | 63%
| Motstander                |  33% |  26% |  19% |  37% | 24%
| *Total*                   | 100% | 100% | 100% | 100% | 100%
| N                         |  403 |  694 |  396 |  116 | 1609
|===

Tabellen viser at hypotesen om at økt salgsutsatthet gir en mer
negativ holdning til direktesalg ikke bekreftes av materialet.

==== Holdning og kjøp (Mål 14)

Følgende tabell viser sammenhengen mellom holdning
til direktesalg og antall kjøp.

[cols="22,20,~,19,12,10"]
.Holdninger til direktesalg og antall angrefristkjøp (A-utvalget)
|===
6+^|Holding til direktesalg:
| Antall kjøp: | Positiv/ nøytral | Skeptisk | Motstander | Total | N
| Ingen/ uoppgitt |  86% |  75% |  86% | 80% | 1294
| Ett          |  11% |  19% |  12% | 15% |  246
| Flere        |   3% |   7% |   2% |  4% |   69
| *Total*      | 100% | 101% | 100% | 99% |
| N            |  186 |  775 |  648 |     | 1609
|===

Denne tabellen viser at det ikke er noen lineær sammenheng mellom
antall kjøp og holdning til direktesalg.  Imidlertid er gruppen
«skeptiske» noe oftere kjøpere enn de andre. Dette avviket, selv om
det er lite, kan ikke forklares ut fra de hypoteser som er stilt opp
foran.

Holdningene i utvalget er entydige: De fleste er skeptiske eller
motstandere av dør-, selskaps- og telefonsalg, - og ulempene ved
dørsalger vesentlig flere enn fordelene. Likevel viser tabell 5.10 at
nær 20% foretar slike kjøp og at kjøpsfrekvensene er like store
både for gruppen som er nøytrale eller positive til direktesalg og har
mer skeptiske holdninger.

Mål 14 om at anskeptisk holdning til direktesalg begrenser antallet
slike kjøp blir ikke bekreftet i materialet.

==== Kjøp og salgsutsatthet (Mål 15)

Nå kan det være at «der ofte er tale om meget dygtige sælgere, som
udnytter uerfarne forbrugere», for å sitere fra medlemsforslaget fra
Nordisk Råd. Undersøkelsen begrenser seg til å gi grunnlag for å
vurdere om økt salgsutsatthet gir flere kjøp. Sammenhengen mellom
disse to variablene er gitt i tabell 6.10.

.Salgsutsatthet og antall angrefristkjøp
|===
.2+| Antall kjøp: 4+| Salgsutsatthet:    .2+|Total .2+| N
                |   0p |   1p |   2p | 3/4p
| Ingen/ uoppgitt |  96% |  80% |  69% |  66% |  80% | 1294
| Ett           |   3% |  17% |  23% |  21% |  16% |  246
| Flere         |   1% |   3% |   8% |  13% |   4% |   69
| *Total*       | 100% | 100% | 100% | 100% | 100% |
| N             |  403 |  694 |  396 |  116 |  100 | 1608
| Prosent       |  25% |  43% |  25% |   7% |      | 100%
|===

Tabellen viser at ved økende salgsutsatthet stiger andel kjøpere fra
20% til 34% og andelen som har foretatt flere kjøp stiger fra 3% til
13%.  Hypotesen om at antallet kjøp stiger med salgsutsattheten
bekreftes i dette materialet.

==== Holdning og misnøye med kjøp (Mål 16)

Holdningen til salgsformene kan påvirkes av tidligere kjøpserfaringer.
Hvis man har dårlige erfaringer, vil dette kanskje kunne føre til at
skepsisen øker.

.Holdning og kjøpsmisnøye
|===
|                3+|Kjøps­misnøye                 .3+v| Alle .3+v| N
|                2+| Fornøyde | Mis­fornøyde
| Holdning:        |   0 |  1 | 2/3
| Positiv/ nøytral |  11% |  11% |   5% |  10% | 72
| Skeptisk         |  69% |  60% |  57% |  62% | 455
| Mot­stander       |  20% |  29% |  37% |  28% | 205
| *Sum*            | 100% | 100% | 100% | 100% |
| N                | 228      | 335      | 169      |          | 732
|===
 
Tabellen viser at andelen med positiv/nøytral holdning synker fra 11%
til 5% etter økende kjøpsmisnøye. Andelen av motstandere av
salgsformen stiger fra 20 til 37 ost. Med det undersøkelsesopplegg som
er brukt, kan en ikke med full sikkerhet si om denne forskjell skyldes
at kjøperne på forhånd var skeptiske til disse salgsformene eller om
kjøpsmisnøyen har forårsaket en noe mer negativ holdning. Det vil bare
en «før- og etter»-undersøkelse kunne avklare. Imidlertid kan en ta
det utgangspunkt at både de gode og de dårlige kjøpserfaringer
utligner hverandre i sin virkning.  Med det utgangspunkt vil
prosentforskjellene ovenfor fremtre som nettovirkninger, med andre ord
at tabellen direkte viser kjøpsmisnøyens effekt på holdningene.

Tabell 6.11 synes å ai et visst grunnlag for å kunne si at misnøye med
kjøp henger sammen med en mer negativ holdning til direktesalg. Ut fra
en forsiktig vurdering av tabell 6.11 kan en si at kjøpserfaringene i
noen grad påvirker synet på direktesalg. Sammenhengen er entydig men
svak.

==== Drøfting av sammenhengen mellom holdninger utsatthet, kjøp og kjøpserfaringer


I dette avsnittet var formålet å se nærmere på sammenhengene mellom
kjøpserfaring, holdning til direktesalg, salgsutsatthet og antall
kjøp. I fig. 6.1 kan både holdning til direktesalg og kjøp være
avhengige variable. På basis av en todeling av variablene gjengis
sammenhengene i tabell 6.12 og 6.13.

.Prosent av kjøpere som er negative til direktesalg etter kjøpsmisnøye og salgsutsatthet
|===
2.2+|                         2+| Salgsutsatthet .2+| Forskjell .2+| N
                                    | høy | lav
.2+| Misnøye med kjøp | fornøyde    s|  53% s|    53% | 0%  | 587
                      | mis­fornøyde s|  70% s|    73% | -3% | 145
  | Forskjell         |             | -17% | -20% 2+|
  | N                 |             | 370 | 362   |    | 732
|===

Tabellen viser at sammenhengen mellom kjøpsmisnøye og negativ holdning
til direktesalg varierer lite når vi kontrollerer for
salgsutsatthet. Samtidig får vi bekreftet at det ikke er noen slik
sammenheng mellom salgsutsatthet og negativ holdning til direktesalg,
heller ikke når vi kontrollerer for kjøpsmisnøye. - Samspillet mellom
de to uavhengige variablene, kjøpsmisnøye og salgsutsatthet er
ubetydelig: Differansen mellom prosentforskjellene er liten.

.Prosent kjøpere i A-utvalget etter salgsutsatthet og holdning til direktesalg
|===
2.2+|                2+| Holdning til direktesalg .2+| Forskjell .2+| N
                              |  +/- |  - -
.2+| Salgs- utsatthet  | Lav s|  16% s|  13%  | +3% | 1097
                       | Høy s|  37% s|  30%   | +7% | 512
   | Forskjell         |     | -21% | -17% 2+|
   | N                 |     | 630 | 979   |    | 1609
|===

Sammenhengen mellom salgsutsatthet og andel kjøpere trer klart frem,
også ved kontroll for holdning til direktesalg. Andelen kjøpere synker
ikke meget med en mer skeptisk holdning til direktesalg. Tabellen
viser også at den innbyrdes sammenhengen mellom salgsutsatthet og
holdning til direktesalg er svært liten med andelen kjøpere som
avhengig variabel.

Vi har funnet en viss sammenheng mellom det vi har definert som
kjøpsmisnøye og holdning til direktesalg. Samtidig har vi også funnet
at økt salgsutsatthet ikke øker andelen skeptiske til direktesalg. -
Holdningen til direktesalg har igjen liten betydning for antall kjøp,
mens antall kjøp påvirkes av salgsutsatthet.

Skulle man gå nærmere inn på mulige årsakssammenhenger kunne kjeden
stilles opp som i fig. 6.2.

.Oppsummering av mulige årsakssammenhenger
image::images/oppsummering-sammenhenger.svg[scaledwidth=60%,align="center"]

Vi har funnet noe støtte for målformuleringene 15 og 16, men ikke
funnet tilsvarende sammenhenger for 13 og 14. I kapittel 7 og 8 vil vi
ta opp spørsmålene om hvilke andre konsekvenser kjøpserfaringen kan
få, for eksempel i form av lesing av angrefristskjema og henvendelse
til selger.

Funnene reiser spørsmålet om hvorfor folks uttrykte skepsis mot
direktesalgsformene i så liten grad får praktiske konsekvenser i form
av færre kjøp.

En forklaring kan være at direktesalgsformene er så effektive at de
overvinner også den kjøpemotstand som måtte ligge i en skeptisk
holdning til omsetningsformen. At det kan være vanskelig å stå imot
tilbudene fremgår også av svarene på spørsmålet om ulemper ved dørsalg
i tabell 5.8. Over 40% i A-utvalget avga svar i gruppene
«uoverlagte impulskjøp» og «vanskelig å si nei».

En annen mulighet er selvfølgelig at de holdninger folk har gitt
uttrykk for ikke sitter så dypt, det kan mer være en «mening» folk har
enn en «holdning». Derfor gir den seg heller ikke atferdsmessig
uttrykk i en kjøpssituasjon. Tabell 5.6 viser imidlertid at
forbrukerne gir ganska skarpt uttrykk for sitt syn på
salgsformene. For hver salgsform har den største gruppen valgt «bør
forbys» som sitt svar, tilsammen kan 40% av A-utvalget
karakteriseres som motstandere av disse salgsformene.  Et uttrykk for
at dette er en vel innarbeidet holdning er at en rekke hushold har
anskaffet egne skilt der de ber om å slippe dørsalg.

En kan vel konkludere med at den første forklaringen er mest
nærliggende.  En skeptisk holdning til omsetningsformene har begrenset
mulighet til direkte å påvirke kjøpssituasjonen i et selskapssalg og
ved et selgerbesøk i hjemmet.

I og med at vi finner en sammenheng mellom dårlige kjøpserfaringer og
en skeptisk holdning til direktesalg, kan holdningen til direktesalg
forstås som en aggregering av egne og det en har sett av andres
erfaringer med disse salgsformene. Om holdningene i hovedsak sees som
en stum av slike kjøpserfaringer, forutsetter ikke det et samsvar
mellom skeptisk holdning og senere kjøpsatferd. Dårlige
kjøpserfaringer synes først og fremst å gi en mer negativ holdning til
direktesalg og da ikke nødvendigvis ikke færre kjøp: Holdningen
justeres, i mindre grad atferden.
