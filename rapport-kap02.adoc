
== Hvordan fikk vi loven om angrefrist

=== Innledning

Formålet med dette kapittel er å gi en oversikt over den prosess som
førte til at angrefristen ble innført for visse kjøp. Oversikten
konsentrerer seg om de formelle sidene ved denne prosessen, om de
interessegrupper som deltok og de viktigste argumentene som ble
fremført. En slik gjennomgang kan forenkle forståelsen av kapittel 3
der blant annet målformuleringer, eller oftere hypoteser om virkninger
av retten til å heve et kjøp, blir drøftet. Det burde også bli enklere
å se sammenhengen når det senere i rapporten vises til uttalelser og
synspunkter som kom fram i løpet av prosessen.

=== Kildemateriale

For å beskrive hvordan denne loven ble til, må man i hovedsak bygge på
det skriftlige materiale som foreligger, fra årsskifte 1965/66 til
loven ble vedtatt i 1972. De viktigste dokumentene i
lovgivningsprosessen er:

Vedtaket i Forbrukerrådet:: Forslag fra Forbrukerrådet om en
betenkningstid i forbindelse med handelslovens
borttingingsbestemmelser fra januar 1966footnote:[Forslaget er
gjengitt i «Medlemsförslag om införande av en regel om betånketid:
Avtalslagen». Nordiske Rådet. 15.  sessionen 1967 17 133/j. Side
20-21.].

Det grunngitte spørsmålet:: _Grunngitt spørsmål i Stortinget_
+
Grunngitt spørsmål i Stortinget fra representanten Borghild Bondevik
Haga 1.  juni 1966 om en vil kunne vente å få en endring i de regler
som nå gjelder for omførselshandel slik at det gis en tidsfrist før en
avbetalingskontrakt blir gyldigfootnote:[Stortingsforhandlinger
1965-66. 1. juni 1966. Side 3140-3141.].

Medlemsforslaget i Nordisk råd::
_Medlemsforslag i Nordisk Råd med høringsuttalelser og betenkning_
+
Et medlemsforslag datert 26. april 1966 i Nordisk Råd om innføringen
av en regel om betenkningstid i avtaleloven fra 1967. Til forslaget
fulgte høringsuttalelser fra myndigheter og organisasjoner i de
nordiske land og en betenkning fra Nordisk Råds juridiske komite om
sakenfootnote:[Jamfør note 1.].

Lovutvalget::
Justisdepartementet nedsatte 12. juli 907 ét eget lovutvalg til å
undersøke behovet for å overveie innholdet av en beskyttelsesregel som
gir kjøperen rett til å tre tilbake fra visse avtaler innen en kortere
fristfootnote:[«Innstilling om fragåelse av kjøp mv. i visse
tilfelle». Innstillingen avgitt 12. november 1970. Side 6.].

Utvalgsinnstillingen:: «Innstilling om fragåelse ved kjøp m.v. i visse
tilfelle» ble avgitt i november 1970.

Høring:: _Høringsuttalelser_
+
Innstillingen ble sendt til høring og det kom inn 26 uttalelser til
Justisdepartementetfootnote:[Uttalelsene foreligger som brev i
Justisdepartementet.].

Ot.Prp. nr. 14:: _Odelstingsproposisjonen_
+
«Ot.prp. nr. 14 (1971-1972) Om lov om angrefrist ved visse avtaler om
forbrukerkjøp:» ble utarbeidet i Justisdepartementet og framlagt for
Stortinget i oktober 1971.

Komiteinnstillingen:: _Innstilling fra Stortingets administrasjonskomité_
+
Lovforslaget ble behandlet i Stortingets administrasjonskomite, som
avga «Innst. 0.X.  (1971-1972) Innstilling fra administrasjonskomiteen
om lov om angrefrist ved visse avtaler om forbrukerkjøp.»

Behandlingen I Stortinget:: _Loven_
+
Den endelige behandlingen i Stortingets organer er gjengitt i
«Forhandlinger i Odelstinget nr. 30 1972. 14.  mars - Lov om
angrefrist ved visse avtaler ved forbrukerkjøp.» (VEDLEGG 3 OG 4)
+
Videre foreligger det henvendelser direkte til Stortinget under
lovbehandlingen og presseomtale i dagsavisene.

Lovkommentaren:: Professor Kristen Andersen utga en kommentarutgave
til loven «Angrefristlov og forbrukerkjøp» i 1974footnote:[ANDERSEN
Kristen «Angrefristlov og forbrukerkjøp». Johan Grundt, Tanum
Forlag, Oslo 1974.].

Dette kapitlet er bygget på de offentlige dokumentene som er nevnt
ovenfor. Videre i undersøkelsen er dokumentene omtalt med de
betegnelser som er gitt ovenfor.

I tillegg kunne det vært ønskelig å supplere dette materialet med
samtaler med de personer som var aktivt med i behandlingen av
saken. Da det skriftlige materialet må sies å være fyldig, synes det
ikke nødvendig å foreta en slik utvidelse av undersøkelsen. De mest
aktuelle spørsmål i denne rapporten er lovens virkninger, og da er det
vurderinger og synspunkter som kom til uttrykk i tiden før loven ble
endelig utformet som må tillegges størst vekt.

Framstillingen i dette kapittel er begrenset til å beskrive enkelte
hovedtrekk ved utarbeidelsen av loven.

=== Utgangspunktet

De problemer forbrukerne møter ved direktesalg er beskrevet i et
vedtak i Forbrukerrådet i januar 1966:

[quote]
«Av de klagesaker som kommer inn til Forbrukerrådet har rådet et
bestemt inntrykk av at borttinging av kjøpmannsvarer direkte til
forbrukerne reiser mange problemer. Vi får mange klagehenvendelser som
særlig går ut på at forbrukerne på grunn av aktive selgere er kommet i
skade for å kjøpe varer de egentlig ikke har bruk for og at de av den
grunn ønsker å annulere salget.  Slike henvendelser gjelder i første
rekke kjøp på avbetaling og dreier seg for øvrig om all slags
kjøpmannsvarer fra bøker til symaskiner. Andre klagemål går ut på at
varen slett ikke har de egenskaper selgeren lovet eller at den viser
seg å være dårligere eller dyrere enn varer av tilsvarende slag som er
til salgs i butikkene....»

Den umiddelbare foranledning ifølge utvalgsinnstillingen var,
tilfeller av dørsalg, der særlig selgere av amerikanske og engelske
bokverk (spesielt leksika) uanmodet oppsøkte kjøpere i deres
hjem. Kjøperene følte seg etter sigende ført bak lyset fordi de trodde
de mottok gratis et større bokverk som ledd i forlagets
forbrukerundersøkelse e.l. Dessverre kostet selve den periodiske
publikasjon kjøperen måtte abonnere på omtrent det samme som et
bokverk og et abonnement til sammen.

Flere av dokumentene omtaler presseomtalen av uheldige dørsalgsmetoder
som et viktig utgangspunkt for å ta opp saken.

I Stortinget reiste representanten Borghild Bondevik Haga i juni 1966
et grunngitt spørsmål om endring i reglen for omførselshandel med
avbetalingskontrakt slik at det kunne gis en tidsfrist før slike
kontrakter ble gyldige. Justisminister Elisabeth Schweigaard Selmer
viste i sitt svar til det nettopp framlagte medlemsforslag i Nordisk
Råd.

=== Medlemsforslaget i Nordisk Råd

I april 1966 foreslo tre medlemmer i Nordisk Ministerråd, Dagmar Ranmark
fra Sverige, Lis Groes: fra Danmark og Ragnar Christiansen fra Norge

[quote]
«at Nordisk Råd henstiller til regjeringene at undersøge
mulighedenefor at få en bestemmelse optaget i aftalelovgivningen og
i lovgivningen om Köb på afbetaling, som giver Köberen en ret til at
træde tilbage fra aftalen inden for en kortere frist».

Forslaget til Nordisk Råd ble sendt til høring i medlemslandene. 39
uttalelser ble avgitt, 11 fra Danmark, 7 fra Finland, 12 fra Norge og
9 fra Sverige. Disse uttalelsene gir et nyansert spektrum av
vurderinger om «dyktige selgere, som udnytter uerfarene forbrukere» og
om det bør innføres en «ret for köberen til at træde tilbage fra
aftalen inden for en vis kortere frist, for eksempel 3-5 dage».

I noen av uttalelsene ble det i argumentasjonen framsatt enkelte
hypoteser om hvilke virkninger en slik fragåelsesrett kunne få. Disse
hypoteser er drøftet i tilknytning til kapittel 3.

Nordisk Råds juridiske komite behandlet medlemsforslaget på møter i
oktober 1966 og februar 1967. Komiteen sammenfatter høringsuttalelsene
slik:

«Forslaget anbefales av Arbejderbevægelsens erhvervsråd, Handels och
Industriministeriet, Tjanestemannaorganisationernas sentralforbund,
Justisdepartementet, Departementet for familie- og forbrugersager,
Den norske advokatforening, Tjånstemånnens centralorganisation og
Landsorganisationen i Sverige.

Tjånstemånnsorganisationens centralforbund fremhæver dog, at hele den
finske handelslovgivning trænger til revision og at indførelsen af en
«fortrydelsesparagraf» vil være et betænkeligt lapperi.

Justisdepartementet påpeker, at sagen har vært fremdraget i
Stortinget, og at det fra flere sider er ytrer utilfredshed med, at
den nuværende lovgivning ikke indrømmer køberen en betænkningstid ved
dørsalg.

Forslaget anbefales ikke af Industrirådet, Assurandørsocietetet,
Advokatrådet, Den danske Handelsstands Fællesrepresentation,
Butikhandelens Fællesråd, Den danske Forlæggerforening,
Centralhandelskammaren, Centrallaget for handelslagen i Finland,
Detaljhandelns centralforbund, Forsikringsrådet, De norske livsforsikringsselskapers forening, Norske forsikringsselskapers forbund,
Den norske forleggerforening, Stockholms handelskammare, Sveriges
køpmannaforbund og Motorbranschens riksforbund.

Imod forslaget anføres nevnlig, at det vil rokke tilliden til
aftalers bindende kraft og være til skade for omsætningen, også selv
om forslaget kun gennomføres på et begrenset område. Der vil opstå
hyppige retstvister, og der er ikke behov for en
«fortrydelsesparagraf» Køberne, specielt afbetalingskøberne er i
forvejen værnet en betenkningstid. Det er iøvrigt tvilsomt, om en
bestemmelse som den forslåede ikke vil få virkninger, som forbrugerne
ikke kan være tjent med. Særlige forhold medfører, at bestemmelsen
ikke kan forenes med de eksisterende lovbestemmelser og praksis
vedrørende forsikringskontrakter.

Følgende myndigheder m.v. anbefaler en begrænset utredning:
Justitsministeriet, Forbrugerrådet, Håndværksrådet,
Handelsdepartement, Forbrukerrådet, Norges Handelsstands forbund,
Norges kooperative landsforening, Svea hovratt og
Kronofogdemyndigheten i Stockholm.

Justisministeriet, Håndværksrådet og Forbrugerrådene i Danmark og
Norge mener, at undersøgelsen bør begrænses til de tilfælde, hvor
sælgeren uanmodet retter henvendelse på Køberens bopæl eller lignende
sted, men den bør foruden køb og salg også omfatte alle andre former
for gensidigt bebyrdende aftaler, herunder særlige forsikringsaftaler,
abonnementsaftaler og serviceavtaler.

Forbrugerrådet påpeger, at de uheldige forhold tillige forekommer, når
sælgerne organiserer sig og opretter standarkontrakter, som
øjeblikkelig binder køberen, men giver sælgeren en frist for akcept.

Handelsdepartementet og Forbrugerrådet mener, at misbrug særlig
forekommer ved afbetalingshandel, som ikke afsluttes på faste
utsalgssteder.

Norges kooperative landsforening fremhæver tillige køb og salg af
brugte genstande, særlig automobiler som et område, hvor behov for en
betænkningsfrist er til stede.

Svea hovratt anser en udredning angående salgsformer, som er behandlet
i Konsumtionskreditutredningens betænkning, for at være velmotivert.

Følgende myndigheder m.v. har ikke taget stilling til forslaget eller
har intet at indvende imod det: Justitieministeriet (som vil afvente
remissytringerne angående Konsumtionskreditutrednings betænkning),
Finlands fackføreningars centralforbund, Overståthållarambetet og
Kooperativa førbundet.»

De ulike oppfatninger avspeiles i komiteen ved at flertallet, 8 av
12 medlemmer, gikk inn for følgenderekommendasjon:

«Nordisk Råd henstiller til regeringerne at undersøge behovet for og
overveje indholdet af en beskyttelsesregel nevnlig i de tilfælde, hvor
aftale indgås eller bestilling afgives under den ene parts uanmodede
henvendelse på den anden parts bopæl eller dermed ligestillet sted,»

De resterende 4 konkluderte alternativt:

«at Nordisk Råd ikke foretager sig noet i anledning af
medlemsforslaget om indførelse af en om betænkningstid i aftaleloven.»

Begrunnelsene for de to forslag er gjengitt i Vedlegg 3.  Selve
drøftelsen i komiteen ble referert som «livlig» ved behandlingen i
Nordisk Råd i april samme år.

=== Behandlingen av medlemsforslaget i Nordisk Råd

Ved behandlingen i Nordisk Råd ble det holdt tre innlegg før
avstemningen om forslagene til rekommendasjonfootnote:[Nordisk Råds
15. sesjon 1967. Side 200-203.].

Innleggene ble
holdt av en representant for flertallsforslaget, en for mindretallet og av
den norske justisminister Elisabeth Schweigaard Selmer.

Statsråden gjorde oppmerksom. på at det norske Justisdepartementet i
sin uttalelse til Nordisk Råd om medlemsforslaget hadde ment at det
kunne være nyttig med en utredning av spørsmålet. Videre viste hun til
de uttalelser som forelå fra en rekke interesserte organisasjoner og
institusjoner. De aller fleste hadde uttalt at det burde foretas en
utredning av spørsmålet, og enkelte gav endog uttrykk for at det burde
innføres en form for betenkningstid. Spørsmålet synes etter dette å ha
såvidt stor aktualitet i Norge at det foretas en nærmere undersøkelse
av behovet for og en overveielse 'av innholdet av en eventuell beskyttelsesregel. «Behovet for en lovfestet betenkningsfrist synes
særlig klart hvor avtale inngås eller bestilling opptas på
kredittbasis eller oråremotakerens uanmodede henvendelse i vanlige
forbrukers hjem eller på arbeidsplassen.»

=== Lovutvalget

Tolvte juli 1967 oppnevnte Justisdepartementet et utvalg til å vurdere
spørsmålet om fragåelse av kjøp mv. i visse tilfelle.: Utvalget fikk
følgende sammensetning:

 * Professor dr.juris Kristen Andersen, formann
 * Forlagsdirektør Henrik Groth
 * Administrerende direktør Herman Scheel
 * Avdelingsleder Inger L. Valle.

De tre sistnevnte ble oppnevnt etter forslag fra henholdsvis Den
norske Forleggerforening og Den norske Bokhandlerforening i
fellesskap, Norges Handelsstands Forbund og Departementet for familie-
og forbrukersaker, Sekretær for utvalget var konsulent Arne
Christiansen, Justisdepartementet.

Hvilke synspunkter var representert i utvalget? Ved høringen om
medlemsforslaget i Nordisk Ministerråd uttalte Handelsstandsforbundet
at de «intet hadde å innvende mot at spørsmålet om å gi kjøperen en
betenkningstid blir nærmere utredet», og at utredning bør «begrenses
til dørsalgstilfelle».

De hadde i en tidligere sak «overfor Handelsdepartementet fremholdt
at man bør vurdere nærmere spørsmålet om hvorvidt det ikke bør
innføres et generelt forbud mot dørsalg til privathusholdninger, blant
annet ut fra hensynet til privatlivets fred».  Forbundet hadde videre
«også understreket at alene de faste utsalgssteder kan by
forbrukerne et tilfredsstillende vareutvalg og gi forbrukerne
betryggelse for at de vet hvor de skal henvende seg i tilfelle
reklamasjoner, eller hvis de trenger ytterligere opplysninger om
varens bruk eller egenskaper o.l.»footnote:[«Medlemsforslaget» side
25 og 28.]

Norges Handelsstands Forbund kan på denne bakgrunn nok ikke sies å
representere dørsalgsfirmaene ved oppnevningen, uten at det dermed
betyr at de representerer hensynet til forbrukerne.

Den norske forleggerforening uttalte i september 1966 at

[quote]
«vi vil advare mot en lovfesting av «angrefristen». Vi finner det
ytterst betenkelig at lovgiverne godtar at en bindende underskrift
ikke skal ha full og øyeblikkelig gyldighet».

Tidligere i uttalelsen gis blant annet følgende bakgrunnsopplysning:

[quote]
«I løpet av de siste måneder har dagspressen bragt kritiske
reportasjer om visse utenlandske foretaks «pressesalg» av verker
(store verker med eiendommelige betalingsvilkår). Disse stadig
tilbakevendende angrep i aviser virket skadelig også på norske forlags
virksomhet, og det ble av taktiske grunner nødvendig å oppfordre
våre medlemmer til å innrømme en annulasjonsrett, og denne er nå
innført av alle større verkselgende forlag.»

Av fire medlemmer representerte en forbrukersiden og to
omsetningssiden.

Formannen Kristen Andersenfootnote:[Andersen, K. Side 19.] beskrev
åpningsfasen i utvalgets arbeide slik:

[quote]
«I det hele var det ikke fritt for at det innenfor utvalget i den
innledende fase gjorde seg gjeldende en viss bekymring for at en slik
beskyttelse kunne få en betenkelig preg av lovforankret
barnepikepolitikk det gjelder ikke minst for mitt eget vedkommende.»

Men han tilføyer i neste avsnitt:

[quote]
«Men den nevnte bekymring avtok etter hvert som arbeidet skred fram,
for innen relativt kort tid å vike plassen for en enstemmig forvissning om at det for de egentlige dørsalgs vedkommende var et reelt
behov for lovpolitiske tiltak til vern om kjøperne.»

For å skaffe utvalget et bedre underlagsmateriale, ble FAKTA A/S bedt
om å foreta en intervjuundersøkelse.  Inntil 7 spørsmål ble stillet
til et landsomfattende representativt utvalg på 1 555 personer i juni
1968.  9% hadde kjøpt på avbetaling ved dørsalg i løpet av siste
år. 19% av de 400 som siste år eller tidligere hadde kjøpt på
avbetaling opplyste at de ville ha benyttet seg av en rett til å fragå
avtalenfootnote:[Lovutvalgetsinnstilling. Vedlegg 2.].

Det ble også foretatt en undersøkelse av 147 klagesaker vedrørende
dørsalg i Forbrukerrådets arkiv.

Lovutvalget avga sin innstilling 12. november
1970. Innstillingen konkluderte med et lovforslag som på enkelte punkter var
meravgrenset enn det som senere ble vedtatt i Stortinget. Med ett unntak sto
utvalget samlet bak forslag og vurderinger. Flertallet (Andersen, Groth og
Scheel) ønsket at loven skulle gjelde «i kjøpers bolig», mens mindretallet
(Valle) foreslo «i noens bolig». Mindretallet fremholdt videre at det:

[quote]
«Også kan være sterke grunner som taler for å utvide fragåelsesretten
til å gjelde for alt salg utenom fast utsalgssted, slik som etter den
engelske Hire-purchase Act. Når jeg ikke har gått til et så omfattende
forslag, skyldes det i første rekke at jeg for salg på slike «andre»
steder er i tvil om en fragåelsesrett er det rette virkemiddel sett
fra et forbrukersynspunkt. Jeg er tilbøyelig til å tro at en her bør
innføre strengere restriksjoner.»

Utvalget foreslo også å la en fragåelsesrett omfatte avtale eller
ordre om løpende tilsyn, vedlikehold, undervisning og lignende
tjenester.

=== Høringsuttalelsene og Justisdepartementets behandling

22 institusjoner og organisasjoner uttalte seg om utvalgets
forslag. I tillegg kom det inn uttalelser fra fire foretak som drev
direktesalg av bøker, elektriske husholdningsartikler og tepper. Det
ble spesielt bedt om vurdering av om loven burde gjelde for andre
kontrakter enn det som inngås etter hjemmebesøk og for andre
kontrakter enn kjøp og abonnementsavtaler. Videre ble det bedt om en
vurdering av den foreslåtte områdningsfristen på 7 dager.

I proposisjonen er blant annet høringsuttalelsene drøftet under
følgende punkter:

 * bør det innføres en områdningstid for visse salg?
 * fragåelsesrett eller forbud
 * stedet for beskyttelsen
 * noen andre hovedspørsmål:
   - hvilke kontraktstyper skal loven gjelde for
   - områdingsfristens beregning og lengde
   - unntak for kontantkjøp
   - det etterfølgende oppgjør
   - tilsagn fra selgerens representant

==== Fragåelsesrett

Praktis talt ingen av høringsinstansene motsatte seg prinsippet om en
fragåelsesrett. Fra enkelte av firmaene ble det påpekt at bokselgerne
burde vies spesiell oppmerksomhet. Uttalelsen fra en større
symaskindistributør antyder dette:

[quote]
«Bøkene er, gjennom unntak i avbetalingslovenfootnote:[Ifølge
avbetalingsloven var bøker tillatt solgt med 10% kontantbeløp,
mens den ordinære kontantandel var 35.], det problemkompleks som
kanskje forårsaker nødvendigheten av en lov om
angrefrist. Forbrukerrådet bør kunne opplyse om andre varer eller
tjenester viser et liknende klagevolum og for øvrig motiverer
angrefrist. vi Synes at loven av hensyn til forlagsbransjen bør sette
inn det hvor virkningen kan forventes å bli størst. Vi foreslår derfor
at loven om angrefrist i første omgang gjøres gjeldende for
forlagsbransjen og bøker som nevnt i utvalgets arbeid og først ved
dokumentert senere behov utvides til å omfatte varer.»

==== Fragåelsesrett eller forbud

En rekke av uttalelsene gikk inn for å utvide lovens område, men det
var nokså delte oppfatninger av hvor langt man skulle gå.
Justisdepartementet overveiet blant annet om det istedenfor eller
sammen med en fragåelsesrett burde innføres delvis forbud mot eller
andre restriksjoner i adgangen til salg utenfor fast forretningssted.

På bakgrunn av en betenkning fra Handelsdepartementet fant
Justisdepartementet «ikke å burde ta noe ytterligere initiativ når
det gjelder en innskrenkning av adgangen til salg utenfor fast
forretningssted».

==== Stedet for beskyttelsen

Det var liten uenighet om at fragåelsesretten måtte gjelde i de
tilfelle der kjøperen binder seg under selgerens besøk i kjøperens
hjem.

Et flertall av høringsinstansene sluttet seg til forslaget om at en
fragåelsesrett måtte omfatte salg som foregår i andre hjem enn
kjøperens eget.

En rekke uttalelser tok til orde for at salg på arbeidsplassen burde
likestilles med salg i hjemmene eller at en slik utvidelse bør
overveies.

Justisdepartementet konkluderte da også med at å foreslå at loven
skulle gjelde alle kjøp som finner sted utenfor selgerens faste
forretningssted.

==== Betydningen av høringsuttalelsene

De grupper som var skeptiske til angrefristen før behandling i
Nordisk Råd i 1967, sluttet seg til flertallets forslag. Blant de resterende
høringsuttalelser var det som nevnt endel som ønsket en utvidelse av stedet
for beskyttelsen.  Justisdepartementet tok hensyn til dette ønsket.

Det synes ikke som regjeringsskiftet i mars 1971, da
Borten-regjeringen gikk av i vesentlig grad påvirket utformingen av
lovforslaget. Lovforslaget fra Justisdepartementet i Ot.prp. nr. 14
(1971-1972) av 29. oktober 1971, fulgte, med ett unntak,den ramme som
ble trukket opp i mandatet for lovutvalget. Unntaket var at
abonnementskontraktene ble holdt utenfor.

=== Stortingsbehandlingen

Lovforslaget ble så drøftet i Stortingets administrasjonskomite. Komiteen sluttet seg i det vesentlige enstemmig til
departementets forslag. Det ble foretatt endringer på to punkter:
Angrefristen ble utvidet til 10 dager og man endret også
departementets forslag slik at oppfyllelse fra selgerens side ikke er
et vilkår for at angrefristen avbrytes.

Loven ga da en forbruker rett til å fragå en avtale eller ordre om
visse kjøp etter bestemte kriterier innen utløpet av 10. dag etter
avtalen eller ordren. I korthet må disse fire kriterier være oppfylt:

 * at tingen er kjøpt ved direktesalg (utenfor fast salgssted),
 * at tingen hovedsakelig er til personlig bruk,
 * at selgeren underrettes om avbestilling innen 10 dager etter at
   avtalen eller ordren er inngått, og
 * at varen ikke er betalt fullt ut.

I tillegg ble selgeren pålagt å overlate et formular til kjøperen som
tydelig opplyser om kjøperens rett etter angrefristloven.  Lovutvalget
tilla dette forslaget betydelig vekt, og det ble fulgt opp av
departement og Storting. Denne pliktige rettighetsinformasjonen er
en viktig side ved angrefristsystemet, og vil bli spesielt vurdert i
de følgende kapitler.

Debatten om lovforslaget i Odelstinget var kort: Fem
stortingsrepresentanter og justisministeren deltok. Fra flere av
representantene ble det understreket behov for ytterligere tiltak
kunne være til stede:

 * ved at fristen «først skulle løpe fra den dag varen er kjøperen i
   hende»,
 * ved postordresalg og
 * ved at det ble opplyst om kontantpris, total pris og merkostnad ved
   kjøp på avbetaling

'''

Innføring av en angrefrist er ikke ukjent i tidligere norsk
lovgivning. Kapittel 21 i Kjøpebolken i Magnus Lagabøters landslov har
følgende bestemmelse om kvinners kjøp:

[quote]
«Umyndig (umagi) kan ikke raade for noget kjøp. Det skal vi ogsaa
vite, hvor store kjøp koner skal raade for: bondekone for 1. øres
kjøp, haulds kone for 2 øres kjøp, lendmands (barons) kone for halv
marks kjøp: men om de kjøper større kjøp, da kån det brytes i den
første maaned. Men om hendes mand ikke er hjemme, da kan han bryte det
i den første maaned, efterat han kommer hjem og han vet om det
kjøp.»footnote:[«Magnus Lagabøters Landslov». Oversatt av Absalon
Taranger 1915. Universitetsforlaget 1970. Side 177.]

En angrefrist er ikke bare kjent i forholdet mellom ektefeller. Fra
det klassiske Athen kjenner en institusjonen «graphe paranomon». Den
åpnet for tiltale og dom for å fremme et illegalt forslag i
Folkeforsamlingen selv om forslaget var godtatt der. Graphe paranomon
var et stabliserende hjelpemiddel, ikke bare for å sikre at fornuften
igjen skulle få overhånd, men også for å redusere sannsynligheten for
at følelsene skulle ta overhånd i første omgangfootnote:[ELSTER, Jon
«Ulysses and the Sirenes: A theory of imperfect rationality», Social
Science Information 16(5) https://doi.org/10.1177/053901847701600501 side
504.].

=== Konklusjon

I dette kapittel har det bare vært mulig å
gjengi den formelle del av den prosess som førte fram til angrefristloven.
Følgende forhold synes å ha hatt vesentlig innvirkning på denne prosessen:

 * at det ble gjennomført dørsalgskampanjer på en slik måte at de
   vakte bred presseomtale: «Stadig tilbakevendende angrep i avisene»
   for å sitere Den norske forleggerforening,
 * at Forbrukerrådet klargjorde hvilke problemer forbrukerne møtte ved
   borttinging av kjøpmannsvarer direkte til forbrukerne og anviste en
   løsning: Rett til å annulere en avtalebetalingskontrakt ved
   borttinging slik som i England,
 * at det ble fremmet et medlemsforslag i Nordisk Råd, som førte til
   at den formelle lovgivningsprosess kom i gang, og
 * at stortingsrepresentanter viste en aktiv interesse for saken til
   loven var vedtatt.
