[appendix]
== Konstruksjon av indekser

I analysen er det benyttet følgende indekser:

 * Holdning til direktesalg
 * Salgsutsatthet
 * Lovkunnskap
 * Feilkunnskap
 * Kjøpsmisnøye
 * Svarvillighet (lovkunnskap)

Dette vedlegg gjennomgår konstruksjonen av disse indekser.

=== Holdning til direktesalg

Indeksen bygger på en sammenregning av svarene på spørsmålene 120, 133
og 134 om berettigelse av dørsalg, selskapssalg og telefonsalg etter
følgende mønster:

|===
|              | ok, vet ikke  | bør begrenses | bør forbys
| dørsalg      | 1 indekspoeng | 2 indekspoeng | 3 indekspoeng
| selskapssalg | 1 indekspoeng | 2 indekspoeng | 3 indekspoeng
| telefonsalg  | 1 indekspoeng | 2 indekspoeng | 3 indekspoeng
|===

Dette gir en rangering med verdiene fra 3 til og med 9 med følgende
resultat for utvalgene.

[cols="24,~,~,~,~,~"]
|===
.2+|  2+| C-utvalget .2+| Kjøpere Antall .2+| Prosent .2+| Prosent forskjell
|   Antall | Prosent
| A 3,4 indekspoeng | 186 | 12 |  72 | 10 | - 2
| B 5-7 indekspoeng | 775 | 48 | 455 | 62 | + 14
| C 8,9 indekspoeng | 648 | 40 | 205 | 28 | - 12
| Sum  | 1 609 | 100 | 732 | 100 | |
|===

Gruppe A er i undersøkelsen omtalt som «nøytrale/positive», gruppe B
som «skeptiske» og gruppe C som «motstandere». Gruppe C har minst
uttalt at to av de tre salgsformer bør forbys og at den tredje bør
begrenses.  Gruppe A har mest uttalt at en av salgsformene bør
begrenses mens de andre to er i orden. Gruppe B utgjør mellom gruppen
som minst mener at to av formene bør begrenses eller at en av dem bør
forbys, og som mest mener at to bør forbys eller to begrenses og en
forbys. Det vises for øvrig til kapittel 5 som omhandler
holdningsspørsmålene.


=== Salgsutsatthet

Forbrukerne får i ulik grad og på ulik måte tilbud om å kjøpe «utenfor
fast salgssted». 4 av spørsmålene om dette er tatt med i denne
indeksen. Sammenregningen er foretatt etter følgende opplegg:

[cols="45,15,~,~"]
|===
| .2+| Indeks-poeng | C-utvalg | kjøpere
| *Dørsalgsfrekvens*| Prosent | Prosent
| Aldri, vet ikke | 0 | 31 | 15
| Et par ganger årlig | 1 | 53 | 65
| Månedlig og oftere | 2 | 16 | 20
2+| | 100 prosent | 100 prosent
4+| *Selskapssalg*
| Ikke deltakelse eller tilbud om deltakelse, vet ikke | 0 | 78 | 62
| Tilbud om deltakelse | 1 | 10 | 9
| Deltakelse | 1 | 12 | 29
2+| | 100 prosent | 100 prosent
4+| *Telefonsalg*
| Tilbud | 1 |8 | 10
| Ikke tilbud, vet ikke | 0 | 92 | 90
2+| | 100 prosent (n=1609) | 100 prosent (n=732)
|===

Dette gir en indeks med verdiene fra 0 til 4. Indeksen gir følgende
fordeling av kjøper og C-utvalg:

|===
.2+| 2+| C-utvalg 2+| Kjøpere
| Antall | Prosent | Antall | Prosent
| 0 Indekspoeng | 403 | 25 |  58 |  8
| 1 Indekspoeng | 694 | 43 | 302 | 43
| 2 Indekspoeng | 396 | 25 | 276 | 38
| 3 Indekspoeng | 111 |  7 |  84 | 12
| 4 Indekspoeng |   5 |  - |   2 |  -
| Sum | 1609 | 100 prosent | 732 |101 prosent
|===

For å forenkle indeksen er verdiene 3 og 4 slått sammen i analysen.

=== Lovkunnskap

Svar på spørsmål 112, 113, 115 og 116 er grunnlaget for beregningen av
indeksen for lovkunnskap. . Indeksen er bygget opp etter følgende
mønster:

[cols="~,10"]
|===
|                                                        | Indeks-poeng
| Fristen er på 10 dager                                 | 1
| Andre svar                                             | 0
| Fristen løper fra avtalen inngås                       | 1
| Andre svar                                             | 0
| Fristen gjelder bare når varen ikke er betalt fullt ut | 1
| Andre svar                                             | 0
| Fristen gjelder bare varer                             | 1
| Andre svar                                             | 0
|===

For å forenkle indeksen er gruppene med 3 og 4 indekspoeng slått
sammen til en gruppe. Meget få oppnådde 4 indekspoeng. For kjøpere og
C-utvalg gir denne indeksen følgende fordeling:

|===
.2+| 2+| C-utvalg 2+| Kjøpere
| Antall | Prosent | Antall | Prosent
| 0 Indekspoeng | 814 | 51 | 276 | 38
| 1 Indekspoeng | 415 | 28 | 202 | 28
| 2 Indekspoeng | 252 | 16 | 175 | 24
| 3/4 Indekspoeng | 92 | 6 | 79 | 11
| Sum | 1 609 | 101 | 732 | 101
|===

=== Feilkunnskap

Denne indeks er basert på en sammenregning av feilaktige svar på fire
av de fem spørsmål om lovkunnskap etter følgende mønster:

[cols="~,10"]
|===
|                                                        | Indeks-poeng
| Angrefristen er på 14 dager eller mer                  | 1
| Andre svar   	     	      	    			 | 0
| Fristens starttidspunkt, ved levering			 | 1
| Andre svar		       				 | 0
| Betalingens betydning, ved kontant betaling 		 | 1
| og uavhengig av betalingsform      			 | 0
| Fristen omfatter, varer og tjenester			 | 1
| Andre svar	    	     				 | 0
|===

Fordelingen blir da:

|===
2.2+|                            2+| C-utvalg       2+| Kjøpere
                                  | Antall | Prosent | Antall | Prosent
2+| Ingen og/eller bare feilaktige svar |    814 |  51 | 276 |  38
2+| Ett riktig svar     		      |    451 |  28 | 202 |  28
2+| To riktige svar 		      |    252 |  16 | 175 |  24
2+| Tre/fire riktige svar 	      |     92 |   6 |  79 |  11
2+| Prosentbasis 	   		      |  1 609 | 101 | 732 | 101
|===

Spørsmålsormuleringene under dette punkt tar først og fremst sikte på
å avdekke lovkunnskap og ikke feilkunnskap, slik at enkelte
svarsalternativer ikke bare omfatter feilkunnskap.  Indeksen viser da
økende sannsynlighet for en feilkunnskap ved økende antall
indekspoeng.

=== Indeks for kjøpsmisnøye

Indeksen er konstruert med utgangspunkt i følgende svar:

|===
|                        | Antall svar | Prosent | Indekspoeng

4+| Spørsmål 143: Har det i løpet av _siste år_ hendt at De ikke har
vært helt fornøyd med et slikt kjøp?

| Ja, ikke vært helt fornøyd (minst en gang) | 110 | 15 | 1
| Nei, ikke hendt siste år   | 559 |  76 | 0
| Vet ikke                   |  63 |   9 | 0
| Sum                        | 732 | 100 |

4+| Spørsmål 169: Vurderte De kjøpet sammen med ektefelle nabo eller
andre i løpet av angrefristen med tanke på eventuell avbestilling?

| Ja                         | 264 |  36 | 1
| Nei                        | 439 |  60 | 0
| Husker ikke                |  29 |   4 | 0
| Sum                        | 732 | 100 |

4+| Spørsmål 172: Om De fikk tilbudet en gang til, og igjen kurne
velge, ville De da kjøpt varen på _denne måten_, kjøpt varen i
_butikk_ eller kanskje utsatt eller ikke foretatt kjøpet:

| Kjøpt igjen på denne måten | 398 |  54 | 0
| Kjøpe et annat sted        |  75 |  10 | 1
| Utsatt/ikke foretatt       | 121 |  17 | 1
| Vet ikke                   | 138 |  19 | 1
| Sum                        | 732 | 100 |
|===

Sammenregnet blir det da en indeks med alternativene med fra 0 til 3
poeng. Kjøperne fordeler seg slik på denne indeksen:

|===
|                    | Antall svar | Prosent
| Fornøyde           | 228 | 31
| Ikke helt fornøyde | 335 | 46
| Noe misfornøyd     | 134 | 18
| Misfornøyde        |  35 |  5
|                    | 732 | 100 prosent
|===

[appendix]
== Spørreskjema

image::images/vedlegg-spoersmaal-s1.jpeg[scaledwidth=85%]

image::images/vedlegg-spoersmaal-s2.jpeg[scaledwidth=85%]

image::images/vedlegg-spoersmaal-s3.jpeg[scaledwidth=85%]

image::images/vedlegg-spoersmaal-s4.jpeg[scaledwidth=85%]

image::images/vedlegg-spoersmaal-s5.jpeg[scaledwidth=85%]

image::images/vedlegg-spoersmaal-s6.jpeg[scaledwidth=85%]

[appendix]
== Forskriften
image::images/vedlegg-forskrift.jpeg[scaledwidth=85%]

[appendix]
== Lovteksten
image::images/vedlegg-lov.jpeg[scaledwidth=85%]

[appendix]
== Avtingingssetel
image::images/vedlegg-returskjema-nynorsk.jpeg[scaledwidth=85%]

[appendix]
== Avbestillingsseddel
image::images/vedlegg-returskjema-bokmaal.jpeg[scaledwidth=85%]
