DBLATEX = dblatex

DBLATEX_OPTS = \
	-T simple \
	-b xetex \
	-r data/dblatex-postprocess \
	--xsl-user=data/user_param.xsl \
	--xsl-user=data/xetex_param.xsl \
	-V \
	-p data/pdf.xsl

all: virkninger-angrefristloven.pdf virkninger-angrefristloven.epub

virkninger-angrefristloven.xml: *.adoc rapport-docinfo*.xml
	asciidoctor -b docbook5 -d book rapport.adoc --out-file=$@
virkninger-angrefristloven.pdf: virkninger-angrefristloven.xml
	$(DBLATEX) $(DBLATEX_OPTS) $<
virkninger-angrefristloven.epub: virkninger-angrefristloven.xml
	dbtoepub $^

virkninger-angrefristloven.html: virkninger-angrefristloven.xml
	xsltproc  --encoding UTF-8 \
	    --output $(subst .pdf,.fo,$@) \
	    data/stylesheet-html.xsl \
	    $<

virkninger-angrefristloven-fop.fo: virkninger-angrefristloven.xml
	xsltproc  \
	    --output $(subst .pdf,.fo,$@).new \
	    data/stylesheet-fo.xsl \
	    $<
	xmllint --format $@.new > $@
	$(RM) $@.new

virkninger-angrefristloven-fop.pdf: virkninger-angrefristloven-fop.fo
	fop -c data/fop-params.xconf -fo $(subst .pdf,.fo,$@) -pdf $@ ; \

clean:
	$(RM) *~
distclean:
	$(RM) virkninger-angrefristloven.xml virkninger-angrefristloven.pdf virkninger-angrefristloven.epub
